import React, { Component } from 'react';

function Body2(){
  return(
    <div className = "body2">
      <div className = "tck1" key = "1"></div>
      <div className = "tck2" key = "2"></div>
      <div className = "tck3" key = "3"></div>
      <div className = "tck4" key = "4"></div>
      <div className = "tck5" key = "5"></div>
      <div className = "tck6" key = "6"></div>
    </div>
  );
}

function Foots(){
  return([
    <div className = "foot1" key = "1"></div>,
    <div className = "foot2" key = "2"></div>,
    <div className = "foot3" key = "3"></div>,
    <div className = "foot4" key = "4"></div>,
    <div className = "foot5" key = "5"></div>,
    <div className = "foot6" key = "6"></div>
  ]);
}
function Eye(props){
  function animate(e){
    var target = e.target;
    if (!target.classList.contains("eyes"))
    target = target.parentElement;
    target.classList.add("animate");
    target.children[0].classList.add("animate3");
  }

    function animate2(e){
      var target = e.target;
      if (!target.classList.contains("eyes"))
      target = target.parentElement;
      target.classList.remove("animate");
      target.children[0].classList.remove("animate3");
    }
  return(
    <div className = "eyes" style = {{left:props.left}} onMouseEnter = {(e) =>{animate(e)}} onMouseLeave = {(e) =>{animate2(e)}}>
      <div className = "ineyes" />
    </div>
  );
}

function Eyes(){
  return([
    <Eye left = "10px" key = "1"/>,
    <Eye left = "54px" key = "2"/>
  ]);
}

function Hear(props){
  function toggle(){
    if (props.ugl === "-1"){
      return {
          transform:`rotate(${props.ugl * 35}deg)`,
          left: props.left,
          borderLeft: "5px solid transparent",
          borderRight: "20px solid #55e"
        }
    }
    else{
      return {
          transform:`rotate(${props.ugl * 35}deg)`,
          left: props.left,
          borderRight: "5px solid transparent",
          borderLeft: "20px solid #55e"
        }
    }
  }
  return (
    <div className = "hear" style = {toggle()} >
      <div className = "inhear" style = {{left: props.leftChild, transform:`rotate(${-props.ugl * 35})`}}></div>
    </div>
  );
}

function Hears(){
  return ([
    <Hear left = "-10px" leftChild = "5px" ugl = "-1" key = "1"/>,
    <Hear left = "95px" leftChild = "-10px" ugl = "1" key = "2"/>
  ]);
}

function Hands(){
  return([
      <div className = "hands" key = "1"></div>,
      <div className = "hands2" key = "2"></div>
  ]);
}

function Head(){
  return(
    <div className = "head">
      <div className = "mounth"></div>
      <Eyes />
      <Hears />
    </div>
  );
}

class Sova extends Component{
  render(){
    return(
      <div className = "body">
        <Hands />
        <Body2 />
        <Head />
        <Foots />
      </div>

    );
  }
}

export default Sova
